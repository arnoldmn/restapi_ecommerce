const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const morgan = require('morgan');
const mongoose = require('mongoose');

//environment variable
require('dotenv/config');
const api = process.env.API_URL;

//Middleware
app.use(bodyParser.json);
app.use(morgan('tiny'));

const productSchema =  mongoose.Schema({
    name: String,
    description: String,
    image: String,
    price: Number,
    stockQuantity: Number,
});

const Product = mongoose.model('Product', productSchema);


app.get(`${api}/products`, (req, res)=>{
    const product = {
        id: 1,
        name:'Hair dresser',
        image: 'some url',

    }
    res.send(product);
})

app.post(`${api}/products`, (req, res)=>{
    const product = new Product({
        name: req.body.name,
        description: req.body.description,
        image: req.body.image,
        price: req.body.price,
        stockQuantity: req.body.stockQuantity
        
    })
    product.save().then((createdProduct=> {
        res.status(201).json(createdProduct)
    })).catch((err)=>{
        res.status(500).json({
            err: err,
            success: false
        })  
    })

})

mongoose.connect(process.env.CONNECT_DB, {
    useNewUrlParser:true,
    useUnifiedTopology: true,
    dbName: 'myshop-db'
}).then(()=>{
    console.log('Connection is ready!')
})
.catch((err)=>{
    console.log(err);
})

app.listen(8000, ()=>{
    console.log(api)
    console.log('Server is running http://localhost:8000');
})